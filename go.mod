module bitbucket.org/tiluvy/shippy-cli-consignment

go 1.12

require (
	bitbucket.org/tiluvy/shippy-service-consignment v0.0.0-20191004143926-f00bcb9e115b
	github.com/micro/go-micro v1.18.0
	google.golang.org/grpc v1.25.1
)

replace bitbucket.org/tiluvy/shippy-service-consignment => ../shippy-service-consignment

replace bitbucket.org/tiluvy/shippy-service-vessel => ../shippy-service-vessel

replace bitbucket.org/tiluvy/shippy-service-user => ../shippy-service-user
